import plotly.graph_objects as go

# Make the plots prettier
import plotly.io as pio
pio.templates.default = "plotly_white"


def plot_scatter(x, y, xtitle, ytitle, title, text=None, hovertemplate="", color="#77dd77"):
    fig = go.Figure()
    fig.add_scatter(
        x=x,
        y=y,
        text=text,
        textposition="top center",
        hoverinfo="x+y",
        hovertemplate=hovertemplate,
        mode="markers+text",
        marker=dict(
            color=color,
        ),
    )
    fig.update_layout(
        xaxis=dict(title=xtitle),
        yaxis=dict(title=ytitle),
        title=title,
    )
    return fig
