from collections import defaultdict, namedtuple
import datetime as dt

import numpy as np
import pandas as pd
import plotly.graph_objects as go
import pytz
from tzwhere import tzwhere


TABLE_STYLE = """
<style>
    .summary_table {
        font-size: 12px;
        border-spacing: 0px;
        min-width: 300px;
    }

    .summary_table caption {
        color: #777777;
    }

    .summary_table tr:nth-child(2n+1) {
        background-color: #f5f5f5;
    }

    .summary_table th, .summary_table td {
        padding: 10px;
    }
</style>
"""

DEFAULT_GET_LINK_TEXT = lambda obs: "Détail"
LINK_COL = "_link"
SPECIAL_COLS = [LINK_COL]


def _check_readers_as_col_names(readers):
    for col in readers.values():
        if not isinstance(col, str):
            raise ValueError(f"All readers must be column names. {col} is not.")


def _extract_data_value(x):
    if x["dataType"] in ("LIST", "MULTILIST"):
        return x["valueLabel"]
    val = x["value"]
    if x["dataType"] == "NUMBER":
        return Number(float(val), x.get("unit"))
    return val


def _html_table(content, caption=""):
    if type(content) is not str:
        content = "".join(content)
    return f"""
        {TABLE_STYLE}
        <table class="summary_table">
            <caption>{caption}</caption>
            {content}
        </table>
    """


def _html_link(url, text, *, target="_blank", style="color: #000;"):
    if text is None:
        text = "{text}"  # To be formatted later
    if url is None:
        return text
    return f'<a href="{url}" style="{style}" target="{target}">{text}</a>'


class Number(namedtuple("Number", ["val", "unit"])):
    def __str__(self):
        return f"{self.val} {self.unit}"


class Parcels(dict):
    def read_most_recent(
        self,
        readers,
        together=False,
        remove_none=True,
        get_obs_link_text=None,
        get_parcel_link_text=None,
    ):
        get_parcel_link_text = get_parcel_link_text or DEFAULT_GET_LINK_TEXT
        data = defaultdict(list)

        for _, parcel_obs in self.items():
            parcel_data = parcel_obs.read_most_recent(
                readers=readers,
                together=together,
                get_link_text=get_obs_link_text,
            )
            if parcel_data.empty:
                continue

            link = parcel_obs.html_link(get_parcel_link_text(parcel_obs))
            data[LINK_COL].append(link)
            for _, row in parcel_data.iterrows():
                col = row["column"]
                data[col].append(row["most_recent_value"])
                data[f"{col}__link"].append(row[LINK_COL])

        df = pd.DataFrame(data)
        if remove_none:
            return df.dropna()
        return df

    def summarize_most_recent(self, readers, together=False):
        _check_readers_as_col_names(readers)
        data = self.read_most_recent(
            readers=readers,
            together=together,
            remove_none=False,
            get_obs_link_text=lambda obs: None,
            get_parcel_link_text=lambda parcel: parcel.exp_id,
        )
        html = [
            "<tr>" +
            "  <th>Parcelle</th>" +
            "".join(
                f'<th style="text-align: center;">{label}</th>'
                for label in data.columns
                if label in readers
            ) +
            "</tr>"
        ]
        html.append("</tr>")

        for _, row in data.iterrows():
            html.append(
                f"""
                <tr>
                    <td>{row[LINK_COL]}</td>
                """
            )
            for label in data.columns:
                if label not in readers:
                    continue
                val = row[label]
                link = row[f"{label}__link"]
                cell_html = link.format(text=val) if val else "-"
                html.append(
                    f"""
                    <td style="text-align: center;">
                        {cell_html}
                    </td>
                    """
                )
            html.append("</tr>")

        return _html_table(html)


class ObservationList(list):
    def find_by_exp(self, exp_id):
        return ObservationList([observation for observation in self if observation.exp_id == exp_id])

    def group_by_exp(self, keep_none=False):
        grouped_obs = defaultdict(list)
        for obs in self:
            if keep_none or obs.exp_id is not None:
                grouped_obs[obs.exp_id].append(obs)
        for exp, observations in grouped_obs.items():
            grouped_obs[exp] = ExpObservations(observations)
        return Parcels(grouped_obs)

    def get_by_id(self, id_):
        observations = [obs for obs in self if obs.id == id_]
        if len(observations) > 1:
            raise ValueError(f"Multiple observations have the id '{id_}'...")
        if not observations:
            raise KeyError(f"Cannot find observation with id '{id_}'")
        return observations[0]

    def order_by_date(self, *, reverse=False):
        obs = sorted(self, key=lambda o: o.date)
        if reverse:
            obs = reversed(obs)
        return ObservationList(obs)

    def read_most_recent(self, readers, together=False, get_link_text=None):
        data = self.order_by_date(reverse=True).read_cols(
            readers=readers,
            remove_none=together,
            get_link_text=get_link_text,
        )

        # If together is True and no observation contain all queried columns,
        # the lists are empty
        if data.empty:
            return pd.DataFrame()

        # Otherwise, we find the most recent non-None value
        df_data = {
            "column": [],
            "most_recent_value": [],
            LINK_COL: [],
        }
        for col in readers:
            try:
                row = data[~data[col].isnull()].iloc[0]
            except IndexError:
                row = {
                    col: None,
                    LINK_COL: None,
                }
            df_data["column"].append(col)
            df_data["most_recent_value"].append(row[col])
            df_data[LINK_COL].append(row[LINK_COL])
        return pd.DataFrame(df_data)

    def read_cols(self, readers, remove_none=False, get_link_text=None):
        get_link_text = get_link_text or DEFAULT_GET_LINK_TEXT
        valid_data = defaultdict(list)
        for obs in self:
            data = {}
            for col, reader in readers.items():
                data[col] = reader(obs) if callable(reader) else obs.data.get(reader)

            # data == {
            #     "col1": val1,
            #     "col2": val2,
            #     ...
            # }

            if remove_none and None in data.values():
                continue

            valid_data[LINK_COL].append(obs.html_link(get_link_text(obs)))
            for col, value in data.items():
                valid_data[col].append(value)

        return pd.DataFrame(valid_data)

    def summarize_most_recent(self, readers, together=False, caption=""):
        _check_readers_as_col_names(readers)
        data = self.read_most_recent(readers=readers, together=together)
        if data is None:
            return "Aucune donnée disponible."
        rows = [
            """
            <tr>
                <th style="text-align: left;">Mesure</th>
                <th style="text-align: center;">Valeur</th>
                <th style="text-align: right;">Publication</th>
            </tr>
            """
        ]
        for _, row in data.iterrows():
            rows.append(f"""
                <tr>
                    <td style="text-align: left;">{row["column"]}</td>
                    <td style="text-align: center;">{row["most_recent_value"] or "-"}</td>
                    <td style="text-align: right;">{row[LINK_COL] or "-"}</td>
                </tr>
            """)
        return _html_table(rows, caption=caption)

    def summarize(self, readers, remove_none=False):
        _check_readers_as_col_names(readers)
        data = self.read_cols(
            readers=readers,
            remove_none=remove_none,
            get_link_text=lambda obs: "Consulter",
        )
        if data.empty:
            return "Aucune donnée disponible."

        html = [
            "<tr>" +
            "".join(
                f"<th>{label}</th>"
                for label in data.columns
                if label not in SPECIAL_COLS
            ) +
            "<th>Publication</th>" +
            "</tr>"
        ]

        for _, row in data.iterrows():
            html.append(
                "<tr>" +
                "".join(
                    f"<td>{val or '-'}</td>"
                    for col, val in row.items()
                    if col not in SPECIAL_COLS
                ) +
                f"<td>{row[LINK_COL]}</td>" +
                "</tr>"
            )
        return _html_table(html)

    def summarize_number_units(self):
        col_to_label = {}
        data = defaultdict(set)
        for obs in self:
            for col, val in obs.data.items():
                if isinstance(val, Number):
                    data[col].add(val.unit)
                    col_to_label[col] = obs.data_id_to_label[col]
        html = []
        max_units = max(len(units) for units in data.values())
        for col, units in data.items():
            html.append("<tr>")
            html.append(f'<th style="text-align: left;">{col} ({col_to_label[col]})</th>')
            for unit in units:
                html.append(f"<td>{unit}</td>")
            # Add potentially missing columns
            for _ in range(max_units - len(units)):
                html.append("<td></td>")
            html.append("</tr>")
        return _html_table(html)


class ExpObservations(ObservationList):
    def __init__(self, observations):
        super().__init__(observations)
        obs = self[0]
        self.exp_id = obs.exp_id
        self.farm_id = obs.farm_id

    @property
    def url(self):
        if self.exp_id is None or self.farm_id is None:
            return None
        return f"https://app.landfiles.com/farms/{self.farm_id}/{self.exp_id}"

    def html_link(self, text, **kwargs):
        return _html_link(text=text, url=self.url, **kwargs)


class Observation:
    def __init__(self, data):
        self.id = data["_id"]
        self.exp_id = data.get("linkedUuid")
        self.farm_id = data.get("farmUuid")
        self.date = dt.datetime.strptime(data["createdDate"]["$date"], "%Y-%m-%dT%H:%M:%S.%f%z")
        self.lng = data.get("longitude")
        self.lat = data.get("latitude")
        self.description = data["description"]
        self.data = {
            x["dataId"]: _extract_data_value(x)
            for x in data["data"]
        }
        self.data_label_to_id = {
            x["dataLabel"]: x["dataId"]
            for x in data["data"]
        }
        self.data_id_to_label = {
            x["dataId"]: x["dataLabel"]
            for x in data["data"]
        }

    def __repr__(self):
        return self.id

    def __str__(self):
        return self.id

    @property
    def timezone(self):
        if self.lng is None or self.lat is None:
            return None
        tz = tzwhere.tzwhere().tzNameAt(self.lat, self.lng)
        return pytz.timezone(tz)

    @property
    def url(self):
        return f"https://app.landfiles.com/detailPicture/{self.id}"

    def html_link(self, text, **kwargs):
        return _html_link(text=text, url=self.url, **kwargs)

    def data_from_label(self, label):
        return self.data[self.data_label_to_id[label]]

    def days_since_seeding(self, timezone=None):
        try:
            seed_date = dt.datetime.strptime(self.data["Date de semis"], "%d/%m/%Y")
        except KeyError:
            return None

        tz = pytz.timezone(timezone) if timezone is not None else self.timezone
        if tz is None:
            raise ValueError(
                "Cannot find timezone from coordinates, please provide one"
            )
        seed_date = seed_date.replace(tzinfo=tz)

        return (self.date - seed_date).days

    def read_number(self, col):
        n = self.data.get(col)
        if n is None:
            return n
        return n.val
