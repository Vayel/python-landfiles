import json
import os

from .exp import Exp, ExpList
from .observation import Observation, ObservationList


def load_json(path):
    with open(path) as fd:
        return json.load(fd)


def load_exps(
    folder,
    obs_fname="observations.json",
    obs_data=None,
    exps_fname="exps.json",
    exps_data=None
):
    if obs_data is None:
        observations = load_obs(path=os.path.join(folder, obs_fname))
    else:
        observations = load_obs(data=obs_data)
    observations = observations.group_by_exp()

    if exps_data is None:
        exps_data = load_json(os.path.join(folder, exps_fname))

    return ExpList([
        Exp(exp, observations.get(exp["_id"], ObservationList([])))
        for exp in exps_data
    ])


def load_obs(path=None, data=None):
    if data is None:
        data = load_json(path)
    return ObservationList(map(Observation, data))
