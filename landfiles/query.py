import heapq


def get_most_recent_sorted(exps, measure, default=None):
    observations = []
    for exp in exps:
        # Try and find the most recent observation with a `key` value
        for obs in reversed(exp.observations.order_by_date()):
            try:
                sort_value, _ = obs.data[measure]
            except KeyError:
                pass
            else:
                heapq.heappush(observations, (sort_value, exp.name, obs))
                break
        else: # Cannot find biomass
            if default is not None:
                heapq.heappush(observations, (default, exp.name, None))
    return [heapq.heappop(observations) for i in range(len(observations))]
