class ExpList(list):
    def find_by_name(self, name):
        return [exp for exp in self if exp.name == name]

    def get_by_id(self, id_):
        exps = [exp for exp in self if exp.id == id_]
        if len(exps) > 1:
            raise ValueError(f"Multiple exps have the id '{id_}'...")
        if not exps:
            raise KeyError(f"Cannot find exp with id '{id_}'")
        return exps[0]


class Exp:
    def __init__(self, data, observations):
        self.id = data["_id"]
        self.name = data["name"]
        self.description = data["description"]
        self.observations = observations

    def __repr__(self):
        return f"{self.name} ({self.id})"

    def __str__(self):
        return self.name
