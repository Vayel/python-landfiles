# Analysis of "Essai Metha CC 2019"

Basic Python package to analyze the data of `Essai Metha CC 2019` (group `Métha51`) on
[Landfiles](https://app.landfiles.com/farms/F-85117932-e4b6-4231-96e7-f5b990259e9e/).

## Dependencies

* Python 3.6+

## Installation

```bash
python3 -V # Must be at least 3.6.0

git clone https://gitlab.com/landfiles/data-analysis.git
cd data-analysis
# Suggested: to run in a virtualenv
pip install -r requirements.txt
```

## Usage

Examples are provided in `main.py`:

```bash
# Activate your virtual environment if you have one
# Then:
python3 main.py
```

### Jupyter notebook

Using [Jupyter notebooks](https://jupyter.org/index.html) is convenient to conduct
data analyses. We highly recommend to use it.

If you **don't** work in virtual environment:

```bash
pip install --user notebook
jupyter notebook tutorial.ipynb
```

If you **do** work in a virtual environment:

```bash
# Activate your virtual environment
pip install notebook ipykernel
python -m ipykernel install --user --name=landfiles
jupyter notebook tutorial.ipynb
```

Then, in the top menu, go to `Kernel > Change kernel` and choose `landfiles`.
