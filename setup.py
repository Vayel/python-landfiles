import io
import os

from setuptools import find_packages, setup, Command

# Package meta-data.
NAME = "landfiles"
DESCRIPTION = "A client to get and analyze data from https://landfiles.com/"
LONG_DESCRIPTION_CONTENT_TYPE = "text/markdown"
URL = "https://gitlab.com/Vayel/python-landfiles"
EMAIL = "vincent.lefoulon@free.fr"
AUTHOR = "Vincent Lefoulon"
REQUIRES_PYTHON = ">=3.6.0"
VERSION = None

HERE = os.path.abspath(os.path.dirname(__file__))

with io.open(os.path.join(HERE, "README.md"), encoding="utf-8") as f:
    long_description = "\n" + f.read()

# Load the package's __version__.py module as a dictionary.
about = {}
if VERSION is None:
    with open(os.path.join(HERE, NAME, "__version__.py")) as f:
        exec(f.read(), about)
else:
    about["__version__"] = VERSION


# Where the magic happens:
setup(
    name=NAME,
    version=about["__version__"],
    description=DESCRIPTION,
    long_description=long_description,
    long_description_content_type=LONG_DESCRIPTION_CONTENT_TYPE,
    author=AUTHOR,
    author_email=EMAIL,
    python_requires=REQUIRES_PYTHON,
    url=URL,
    packages=find_packages(exclude=("tests",)),
    install_requires=[
        "plotly>=4.4.1",
        "pytz>=2019.3",
        "tzwhere>=3.0.3",
        "pandas>=1.0.3",
    ],
    include_package_data=True,
    classifiers=[
        # Full list: https://pypi.python.org/pypi?%3Aaction=list_classifiers
        "Programming Language :: Python",
        "Programming Language :: Python :: 3",
        "Programming Language :: Python :: 3.6",
        "Programming Language :: Python :: 3.7",
        "Programming Language :: Python :: 3.8",
    ],
)
